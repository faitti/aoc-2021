#include <bits/stdc++.h>
#define lli long long int
#define lld long long double
#define r(x) cin >> x
#define p(x) cout << x << "\n"
#define LOOP(x,n) for(int x = 0; x < n; ++x)
#define pb push_back
#define um unordered_map
#define vi vector<int>
#define pasi pair<string, int>
using namespace std;
int main()
{
    string gr, er;
    vector<string> inp(2000);
    LOOP(y, 2000)
        r(inp[y]);

    for (int j = 0; j < 12; j++) {
        um<char, int> l = {{'1', 0}, {'0', 0}};
        for (int k = 0; k < 2000; k++) {
            l[inp[k][j]]++;
        }
        bool co = l['1'] > l['0'];
        gr += (co ? '1' : '0');
        er += (co ? '0' : '1');
    }
    p(stoi(gr, 0, 2) * stoi(er, 0, 2));
    return 0;
}

