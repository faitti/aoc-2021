let data = require('fs').readFileSync('/dev/stdin', 'utf-8').split("\n");
let datac = data
const Main = () => {
    let id = 0;
    while (data.length > 1) {
        let m = {"1": 0, "0": 0}
        for (let j = 0; j < data.length; j++) {
            m[data[j][id]]++;
        }
        data = data.filter(c => c[id] == (m["1"] >= m["0"] ? "1" : "0"));
        id++;
    }
    id = 0;
    while (datac.length > 1) {
        let m = {"1": 0, "0": 0}
        for (let j = 0; j < datac.length; j++) {
            m[datac[j][id]]++;
        }
        datac = datac.filter(c => c[id] == (m["1"] >= m["0"] ? "0" : "1"));
        id++;
    }
    console.log(parseInt(data[0], 2) * parseInt(datac[0], 2));
}
Main();
