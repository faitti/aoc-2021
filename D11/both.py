i = {(j, k):int(e) for j, l in enumerate(open('/dev/stdin')) for k, e in enumerate(l.strip())}
ans = 0
def ns(x, y):
    return filter(i.get, [(x+1,y+1),(x+1,y),(x+1,y-1),(x,y+1),
             (x-1,y-1),(x-1,y),(x-1,y+1),(x,y-1)])

for s in range(1, 1000):
    for e in i: i[e] += 1
    fls = {o for o in i if i[o] > 9}
    while fls:
        cur = fls.pop()
        i[cur] = 0
        ans += 1
        for n in ns(*cur):
            i[n] += 1
            if i[n] > 9: fls.add(n)

    if s == 100: print(ans)
    if sum(i.values()) == 0: print(s); break
