from sys import stdin
caves = {}
for l in stdin:
    l = l.strip().split("-")
    if l[0] in caves: caves[l[0]].append(l[1])
    else: caves[l[0]] = [l[1]]
    if l[1] in caves: caves[l[1]].append(l[0])
    else: caves[l[1]] = [l[0]]

def pf(cvs, n, d):
    vis, p, ap = set(), [], []
    def gp(cvs, n, d, vis, p):
        if n.islower(): vis.add(n)
        p.append(n)
        if n == d: ap.append(p.copy())
        else:
            for nn in cvs[n]:
                if nn not in vis: gp(cvs, nn, d, vis, p)
        p.pop()
        if n.islower(): vis.remove(n)
    gp(cvs, n, d, vis, p)
    return ap
print(len(pf(caves, "start", "end")))
