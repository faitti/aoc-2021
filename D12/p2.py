from sys import stdin
caves = {}
for l in stdin:
    l = l.strip().split("-")
    if l[0] in caves: caves[l[0]].append(l[1])
    else: caves[l[0]] = [l[1]]
    if l[1] in caves: caves[l[1]].append(l[0])
    else: caves[l[1]] = [l[0]]

def pf(cvs, n, d, rl):
    vis, p, ap, rv = set(), [], [], False
    def gp(cvs, n, d, vis, p, rv):
        if ((rv == True or n != rl) and n.islower()) or n in ["end", "start"]: vis.add(n)
        elif n == rl and rv == False: rv = True
        p.append(n)
        if n == d: ap.append(p.copy())
        else:
            for nn in cvs[n]:
                if nn not in vis: gp(cvs, nn, d, vis, p, rv)
        p.pop()
        if ((rv == True or n != rl) and n.islower()) or n in ["end", "start"]:
            if n not in vis: rv = False
            else: vis.remove(n)
    gp(cvs, n, d, vis, p, rv)
    return ap

ls, ap = {k for k in caves.keys() if k.islower()}, []
for l in ls: ap.extend(pf(caves, "start", "end", l))
ap = {tuple(p) for p in ap}
print(len(ap))
