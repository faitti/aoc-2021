import fileinput
def pl(l):
    lll, rrr = l.split('|')
    return list(map(frozenset, lll.split())), list(map(frozenset, rrr.split()))

r = { 0: lambda n, r: len(n) == 6, 1: lambda n, r: len(n) == 2, 2: lambda n, r: n, 3: lambda n, r: len(n) == 5 and len(n & r[1]) == 2, 4: lambda n, r: len(n) == 4, 5: lambda n, r: len(n) == 5 and len(n & r[6]) == 5, 6: lambda n, r: len(n) == 6 and len(n & r[1]) == 1, 7: lambda n, r: len(n) == 3, 8: lambda n, r: len(n) == 7, 9: lambda n, r: n == (r[4] | r[3]) }

def ar(p, rr, l):
    return list(filter(lambda n: r[rr](n, l), p))[0]

def cl(lh, rO):
    lo = {}
    p = set(lh)
    for r in rO:
        lo[r] = ar(p, r, lo)
        p.discard(lo[r])
    return {j:k for (k, j) in lo.items()}

def solve(lh, rh, rO):
    lo = cl(lh, rO)
    res = [lo[r] for r in rh]
    res = int(''.join(map(str, res)))
    return res

inp = list(map(pl, fileinput.input()))
rulo = [1, 4, 7, 8, 6, 3, 9, 5, 0, 2]
print(sum([solve(ll, rr, rulo) for ll, rr in inp]))
