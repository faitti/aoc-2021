X = []
LS = [2, 3, 4, 7]
T = 0
with open('inp.txt', 'r') as f:
    for c in f:
        X.append(c.split(" | ")[1].replace("\n", ""))

for e in X:
    for r in e.split(" "):
        if len(r) in LS:
            T += 1

print(T)
