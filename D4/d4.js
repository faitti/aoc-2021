const inp = require('fs').readFileSync('/dev/stdin', 'utf-8').split("\n");
const Main = () => {
    let ns = inp[0].split(",");
    let bngs = []
    let ta = []
    for (let j = 1; j < inp.length; j++) {
        if (inp[j] == '') { bngs.push(ta); ta=[]; continue; }
        ta.push(inp[j].split(' ').filter(c => parseInt(c)));
    }
    for (let e = 0; e < ns.length; e++) {
        for (let b = 0; b < bngs.length; b++) {
            let cn = ns[e]
            let tr = bngs[b].reduce((p, n) => n.map((_, i) => (p[i] || []).concat(n[i])), []);
            for (let r of bngs[b]) {
                if (!(r.includes(cn))) { continue; }
                r[r.indexOf(cn)] = "X";
                if (String(r).replace(/[^X]+/g,'').length == 5) {
                    let s = 0;
                    bngs[b].filter(e => e.filter(c => c != "X" ? s += parseInt(c) : null));
                    console.log(s * parseInt(cn));
                    process.exit();
                }
            }
            for (let r of tr) {
                if (!(r.includes(cn))) { continue; }
                r[r.indexOf(cn)] = "X";
                if (String(r).replace(/[^X]+/g, '').length == 5) {
                    let s = 0;
                    tr.filter(e => e.filter(c => c != "X" ? s += parseInt(c) : null));
                    console.log(s * parseInt(cn));
                    process.exit();
                }
            }
        }
    }
}

Main()
