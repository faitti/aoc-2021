const inp = require('fs').readFileSync('/dev/stdin', 'utf-8').split("\n");
const Main = () => {
    let ns = inp[0].split(",");
    let bngs = []
    let ta = []
    let ws = []
    for (let j = 1; j < inp.length; j++) {
        if (inp[j] == '') { bngs.push(ta); ta=[]; continue; }
        ta.push(inp[j].split(' ').filter(c => parseInt(c)));
    }
    for (let e = 0; e < ns.length; e++) {
        for (let b = 0; b < bngs.length; b++) {
            let cn = ns[e]
            if (bngs[b] == "W") continue;
            let tr = bngs[b].reduce((p, n) => n.map((_, i) => (p[i] || []).concat(n[i])), []);
            for (let r of bngs[b]) {
                if (!(r.includes(cn))) { continue; }
                r[r.indexOf(cn)] = "X";
                if (String(r).replace(/[^X]+/g,'').length == 5) {
                    ws.push([bngs[b], cn]);
                    bngs[b] = "W";
                }
            }
            for (let r of tr) {
                if (bngs[b] == "W") { break; }
                if (!(r.includes(cn))) { continue; }
                r[r.indexOf(cn)] = "X";
                if (String(r).replace(/[^X]+/g, '').length == 5) {
                    ws.push([tr, cn]);
                    bngs[b] = "W";
                }
            }
        }
    }
    let l = ws[ws.length-1]
    let s = 0;
    l[0].filter(e => e.filter(c => c != "X" ? s += parseInt(c) : null));
    console.log(s*l[1])
}

Main()
