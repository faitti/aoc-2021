PROGRAM solve
  REAL, DIMENSION(2000) :: list
  INTEGER :: l
  INTEGER :: c
  INTEGER :: te
  INTEGER :: t = 0
  DO i = 1, 2000
    READ(*,*) te
    list(i) = te
  END DO
  l = list(1) + list(2) + list(3)
  DO n = 3, 1999
    c = list(n-1) + list(n) + list(n+1)
    IF (l < c) THEN
      t = t + 1
    END IF
    l = c
  END DO
  WRITE(6, *) t
END PROGRAM solve
