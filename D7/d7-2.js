const data = require('fs').readFileSync('/dev/stdin', 'utf-8').replace("\n", "").split(',').map(e => parseInt(e))
const solve = () => {
    let best = 10e18
    for (let x = 1; x < Math.max(...data); x++) {
        let cur = 0
        for (let j = 0; j < data.length; j++) {
            let n = Math.abs(data[j] - x)
            cur += ((n * (n + 1)) / 2)
        }
        if (cur < best) best = cur;
    }
    console.log(best)
}
solve()
