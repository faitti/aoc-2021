#include <stdio.h>
#include <math.h>
int main() {
    int c = -1;
    int i = 0;
    int best = (int)1e18;
    int max = -1;
    int in[1000];
    while (c != EOF) {
        scanf("%d", &c);
        in[i++] = c;
        if (c > max) max = c;
    }
    for (int x = 1; x < max; x++) {
        int cur = 0;
        for (int j = 0; j < sizeof(in) / sizeof(in[0]); j++) {
            int n = abs(in[j] - x);
            cur += ((n * (n + 1)) / 2);
        }
        if (cur < best) best = cur;
    }
    printf("%d", best);
    return 0;
}
