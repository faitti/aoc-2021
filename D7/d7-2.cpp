#include <bits/stdc++.h>
#define lli long long int
#define lld long long double
#define r(x) cin >> x
#define p(x) cout << x << "\n"
#define LOOP(x,n) for(int x = 0; x < n; ++x)
#define pb push_back
#define um unordered_map
#define vi vector<int>
using namespace std;
int main()
{
    vi in{1000};
    int bs = (int)1e18;
    int max = -1;
    LOOP(j, 1000) {
        r(in[j]);
        p(in[j]);
        if (in[j] > max) max = in[j];
    }

    for (int k = 1; k < max; k++) {
        lli cur = 0;
        for (int y = 0; y < in.size(); y++) {
            lli n = abs(in[y] - k);
            cur += ((n * (n + 1)) / 2);
        }
        if (cur < bs) bs = cur;
    }
    p(bs);
	return 0;
}

