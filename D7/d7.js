const data = require('fs').readFileSync('/dev/stdin', 'utf-8').split(',').sort((j, k) => j-k);
console.log(data.reduce((t, s) => t += Math.abs(s - data[(data.length/2) >> 0]), 0));
