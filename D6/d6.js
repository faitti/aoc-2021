let data = require('fs').readFileSync('/dev/stdin', 'utf-8').split(",").map(e => e.replace("\n", ""))
const solve = () => {
    for (let j = 0; j < 80; j++) {
        for (let k = 0; k < data.length; k++) {
            if (data[k] == "0") {
                data[k] = "6";
                data.push("9")
            } else {
                data[k] = String(parseInt(data[k]) - 1);
            }
        }
    }
    console.log(data.length)
}
solve()
