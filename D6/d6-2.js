let data = require('fs').readFileSync('/dev/stdin', 'utf-8').replace("\n", "").split(",").map(e => parseInt(e))
const solve = () => {
    let c = []; for (let k = 0; k < 9; k++) { c.push(0); }
    for (let e of data) {
        c[e]++;
    }
    for (let j = 0; j < 256; j++) {
        c[(7+j)%9] += c[j%9];
    }
    let sum = 0;
    for (let x of c) {
        sum += x;
    }
    console.log(sum)
}
solve()
