from sys import stdin
lines = []
points = {"(": 1, "[": 2, "<": 4, "{": 3}
matches = {"(": ")", "[": "]", "<": ">", "{": "}"}
rlist = []
for l in stdin:
    lines.append(l.strip())

for l in lines:
    stack = []
    ans = 0
    for c in l:
        if c in ["(", "<", "{", "["]:
            stack.append(c)
        else:
            if len(stack) < 1: continue
            if not (matches[stack.pop()] == c):
                stack = []
                break
    for e in reversed(stack):
        ans = ans * 5 + points[e]
    if ans > 0:
        rlist.append(ans)
print(sorted(rlist)[int(len(rlist)/2)])
