from sys import stdin
lines = []
wrong = {"}": 0, "]": 0, ")": 0, ">": 0}
points = {")": 3, "]": 57, "}": 1197, ">": 25137}
matches = {"(": ")", "[": "]", "<": ">", "{": "}"}
ans = 0
for l in stdin:
    lines.append(l.strip())

for l in lines:
    stack = []
    for c in l:
        if c in ["(", "<", "{", "["]:
            stack.append(c)
        else:
            if len(stack) < 1: continue
            if not (matches[stack.pop()] == c):
                wrong[c] += 1
                break
for e in points:
    ans += wrong[e] * points[e]
print(ans)
