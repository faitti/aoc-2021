const data = require('fs').readFileSync('/dev/stdin', "utf-8").split("\n");
const Main = () => {
    let x = 0; let y = 0; let a = 0;
    for (let j = 0; j < 1000; ++j) {
        let ci = data[j].split(" ");
        switch (ci[0][0]) {
          case "d":
            a += parseInt(ci[1]);
            break;
          case "u":
            a -= parseInt(ci[1])
            break;
          case "f":
            x += parseInt(ci[1])
            y += parseInt(ci[1]) * a;
        }
    }
    console.log(x*y)
}

Main()
