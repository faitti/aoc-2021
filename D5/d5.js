const data = require('fs').readFileSync('/dev/stdin', 'utf-8').split("\n");
const solve = () => {
    let ls = []
    for (let m = 0; m < 1000; m++) {
        ls[m] = []
        for (let k = 0; k < 1000; k++) {
            ls[m][k] = 0;
        }
    }
    console.log(ls)
    for (let e of data) {
        if (e.length == 0) continue;
        let c = e.split(' -> ').map(x => x.split(','));
        if(!(c[0][0] == c[1][0] || c[0][1] == c[1][1])) continue;
        let p = parseInt(c[0][0]) > parseInt(c[1][0])
            ? [parseInt(c[0][0]), parseInt(c[1][0])]
            : [parseInt(c[1][0]), parseInt(c[0][0])];
        let s = c[0][1]
        let d = "y"
        if (p[0] == p[1]) {
            p = parseInt(c[0][1]) > parseInt(c[1][1])
                ? [parseInt(c[0][1]), parseInt(c[1][1])]
                : [parseInt(c[1][1]), parseInt(c[0][1])]
            s = c[0][0]
            d = "x"
        }
        for (let j = p[1]; j <= p[0]; j++) {
            if (d == "y") {
                ls[s][j]++;
            } else if (d == "x") {
                ls[j][s]++;
            }
        }
    }
    let co = 0;
    for (let e of ls) {
        for (let ee of e) {
            if (ee > 1) co++;
        }
    }
    console.log(co)
}
solve()
