X = []
S = 0
with open('input.txt', 'r') as f:
    for r in f.read().split("\n"):
        X.append([10] + [c for c in r] + [10])
    X.insert(0, [10 for _ in range(len(X[0]))])
    X.append([10 for _ in range(len(X[0]))])

def getn(j, k) -> list:
    ns = []
    for i in range(9):
        if (i != 4):
            try:
                ns.append(int(X[j+i//3-1][k+i%3-1]))
            except IndexError:
                pass
    return ns

for j in range(len(X)):
    for k in range(len(X[j])):
        if X[j][k] == 10:
            continue
        x = getn(j, k)
        c = int(X[j][k])
        S += c+1 if c <= min(x) else 0
print(S)