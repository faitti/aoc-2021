import networkx as nx
import math

with open('inp.txt') as f:
    area = nx.Graph()
    for y, l in enumerate(f.readlines()):
        for x, d in enumerate(l.strip()):
            area.add_node((x, y))
            if x > 0:
                area.add_edge((x, y), (x-1, y))
            if y > 0:
                area.add_edge((x, y), (x, y-1))
            area.nodes[(x, y)]['d'] = d

for x, y in area.nodes:
    if area.nodes[(x, y)]['d'] == '9':
        area.remove_edges_from([
            ((x, y), (x-1, y)),
            ((x, y), (x+1, y)),
            ((x, y), (x, y-1)),
            ((x, y), (x, y+1))
        ])

cs = [len(c) for c in sorted(nx.connected_components(area), key=len, reverse=True)]

print(math.prod(cs[:3]))
